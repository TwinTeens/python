import os
import threading
from flask import Flask, json, request,send_file
from flask_restful import Resource, Api
from flask_pymongo import PyMongo
from flask_api import status
import config
import uuid
from flask_mail import Mail, Message
from os.path import join, dirname, realpath
from werkzeug.utils import secure_filename
from flask_cors import CORS
import dateutil.parser as dp
import calendar;
import time;
from base64 import b64encode, b64decode
import ast
import tabula 
import pandas as pd
import math
from xlrd import open_workbook
from tabulate import tabulate
import re
from PyPDF2 import PdfFileReader
from io import BytesIO
import shutil
import camelot
import pdfkit
from fuzzywuzzy import fuzz
import numpy as np
from specComparison import compareSpec
#from docx import Document


UPLOADS_PATH = join(dirname(realpath(__file__)), 'uploads/')


app = Flask(__name__)
app.config.from_object('config.DevelopementConfig')
mailUserName = app.config['MAIL_USERNAME']
host = "https://bidtoaster.com/"
mongo = PyMongo(app)
api = Api(app)
mail = Mail(app)
CORS(app)

textPhrases = {
    "description" : ["description","specifications","configurations","product details","services details","product description","specification","specificatio"],
    "unit" : ["unit price/year","unit cost/year","unit rate/year","nrc","nrc (inr)","(nrc inr)","nrc inr","unit price"],
    "rate" : ["rate","price","cost","rates","prices","costs","pricing","amount","rate/year","cost/year","amount/year","arc","arc (inr)","(arc inr)","arc inr","total price","total"],
    "quantity" : ["quantity","qty","quantities","volume","qty."]
    #"tax" : ["tax","vat","gst","service tax","cst"]
}

stopwords = ['The','what', 'who', 'is', 'a', 'at', 'is', 'he','with','and','has','are',"for","specs","specification","years","international","warranty","laptop","Generation"]
keywords=['processor','hdd','memory','core','GB','server','disk','ram','rom']
excelColumns = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
#Used to send the mail
def sendMail(receivers,message,body):
        sender = mailUserName
        receivers = receivers
        #message = message'Registration confirmation'
        try:
            msg = Message(message, sender =sender, recipients = receivers)
            msg.html = body #"Thanks for registration! \n \n Use this link for RFP creation http://localhost:5002/rfp/create/" + str(id)
            def sendAsyncMail(msg):
                with app.app_context():
                    try:
                        mail.send(msg)
                        print("Successfully sent email")
                    except:
                        print("Error: unable to send email")
            mailThread = threading.Thread(name='mail_sender', target=sendAsyncMail, args=(msg,))
            mailThread.start()
        except:
            print("Error: unable to send email outline")

def isExpiry(bidEndtime):
        ts = calendar.timegm(time.gmtime())
        if ts < int(bidEndtime):
            return False
        else:
            return True

def convertToInteger(d):
        n = ""
        for l in d:
            if l.isalnum() and l.isdigit():
                n+=l
        return int(n)

class Login(Resource):
    def get(self):
        if "access_key" in request.headers:
            accessKey = request.headers["access_key"]
            accessData = json.loads(b64decode(accessKey))
            user = mongo.db
            user.logins.delete_one(accessData)
            return {"status":"success","message":"Logged out successfully"}
        else:
            response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
            return response,status.HTTP_401_UNAUTHORIZED
    def post(self):
        data = json.loads(request.data)
        userDB = mongo.db
        usersList = userDB.auth.find({"accountType":data['accountType'],"email":data['username']})
        if usersList.count() == 0:
            return{"status":"error","message":"Username and password doesn't match! Please enter registered username and password"}
        else:
            for i in usersList:
                if(i["email"] == data["username"]):
                    if(i["password"] == data["password"]):
                        if(i['confirmation_status']==True):
                            loginData = {}
                            ts = calendar.timegm(time.gmtime())
                            accInfo = {"accountType":data["accountType"],"email":data["username"],"login_time":ts,"id":i['id']}
                            #first json should need to be converted to string and encode to ascii
                            accInfoBytes = json.dumps(accInfo).encode('ascii')
                            loginData['access_key'] = b64encode(accInfoBytes).decode('ascii')
                            loginData['firstName'] = i['firstName']
                            loginData['lastName'] = i['lastName']
                            userDB.logins.insert(accInfo)
                            return{"status":"success","message":"valid user","data":loginData}
                        else:
                            return{"status":"error","message":"Mail confirmation is pending for the user"}
        return{"status":"error","message":"Incorrect user or password"}

class Users(Resource):
    def get(self,id):
        userDB = mongo.db
        data = userDB.auth.find({'id':id})
        if data.count() == 0:
            response = {"status":"error","message":"User details not found to authorize the account"}
            return response
        for i in data:
            userDB.auth.update_one({'_id':i['_id']},{'$set':{'confirmation_status':True}},upsert=False)
        return{"status":"success","message":"Your mail is confirmed. You can login to continue with us"}
    def post(self):
        data = json.loads(request.data)
        userDB = mongo.db
        if userDB.auth.find({"email":data['email']}).count() > 0:
            return{"status":"error","message":"Mail id already exists."}
        random_id = str(uuid.uuid4())
        data['id'] = random_id
        self.checkVendorRfp(data)
        data['confirmation_status'] = False
        userDB.auth.insert(data)
        receivers = [data['email']]
        message = "Mail Confirmation"
        html = "<p>Dear "+ data['firstName']+ ",</p><p>Thank you for registering with us.</p><p>Please click <a href='"+host+"confirmation/" + random_id +"'> here </a> to confirm your mail and activate free account.</p><br><p>Warm Regards,</p><p>Procurement Team</p>"
        #body = "Dear "+ data['firstName']+ ", \n \n Thank you for registering with us. \n \n Please confirm your mail and activate account by click on the link <a href='http://localhost:8082/confirmation/'" + random_id +" here </a> \n \n Warm Regards, \n \n Procurement Team"
        sendMail(receivers,message,html)
        return{"status":"success","message":"Confirmation mail has been sent to your mail. Please click on the link to confirm."}

    def checkVendorRfp(self,data):
        db = mongo.db
        if data['accountType'] == 'buyer':
            return
        rfpList = db.rfp.find()
        for rfp in rfpList:
            if data['email'] in rfp['newVendorsList']:
                vData = {}
                vData['id'] = data['id']
                vData['email'] = data['email']
                vData['name'] = data['firstName']
                db.rfp.update({'_id':rfp['_id']},{'$push':{'oldVendorsList':vData}})
                

class Vendor(Resource):
    def get(self):
        if "access_key" in request.headers:
            accessKey = request.headers["access_key"]
            #if accessKey
            accessData = json.loads(b64decode(accessKey))
            vendors = mongo.db
            if(vendors.logins.find(accessData)).count() == 1:
                output = []
                for user in vendors.auth.find({"accountType":"vendor"}):
                    del user['_id']
                    output.append({"id":user['id'],"name":user['firstName']+" "+user['lastName'],"email":user['email']})
                response = {"total":vendors.auth.count(),"data":output}
                return json.loads(json.dumps(response, default=str))
            else:
                response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
                return response,status.HTTP_401_UNAUTHORIZED
        else:
            response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
            return response,status.HTTP_401_UNAUTHORIZED

    def post(self):
        if "access_key" in request.headers:
            accessKey = request.headers["access_key"]
            #if accessKey
            accessData = json.loads(b64decode(accessKey))
            user = mongo.db
            if(user.logins.find(accessData)).count() == 1:
                userId = accessData['id']
                random_id = str(uuid.uuid4())
                dirPath = os.path.join(UPLOADS_PATH, userId)
                if not os.path.exists(dirPath):
                    os.mkdir(dirPath,mode=0o777)
                data = json.loads(request.form.get('data'))
                for key,value in data['fileUploadDetails'].items():
                    if value == None:
                        print("none",key)
                    else:
                        f = request.files[key]
                        filename = secure_filename(f.filename)
                        f.save(os.path.join(dirPath, filename))
                
                vendors = mongo.db
                data['id'] = random_id
                data['user_id'] = userId
                bank_data = {}
                bank_data['id'] = random_id
                bank_data['bank_details'] = data['bankDetails']
                self.savebankdetails(bank_data)
                del data['bankDetails']
                vendors.users.insert(data)
                receivers = [data['companyDetails']['regEmail']]
                message = "Supplier Registration Confirmation"
                body = "Thanks for the registration!"
                sendMail(receivers,message,body)
                #self.sendregisterconfirmationmail(data['companyDetails']['regEmail'],random_id)
                response = {"status" : "success", "message":"Supplier is registered successfully and confirmation mail is sent to the registered email"}
                return response
            else:
                response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
                return response,status.HTTP_401_UNAUTHORIZED
        else:
            response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
            return response,status.HTTP_401_UNAUTHORIZED

    def savebankdetails(self,bank_details):
        vendors = mongo.db
        vendors.user_bank_details.insert(bank_details)


class RFP(Resource):
    def get(self,rfp_id):
        return {"status":"success","message":"user allowed to create RFP"}
    def post(self):
        if "access_key" in request.headers:
            accessKey = request.headers["access_key"]
            #if accessKey
            accessData = json.loads(b64decode(accessKey))
            user = mongo.db
            if(user.logins.find(accessData)).count() == 1:
                userId = accessData['id']
                data = json.loads(request.form.get('data'))
                filename = ""
                data['user_id'] = userId
                vendors = mongo.db
                random_id = str(uuid.uuid4())
                data['rfp_id'] = random_id
                if "rfpFile" in request.files:
                    f = request.files['rfpFile']
                    dirPath = os.path.join(UPLOADS_PATH,userId)
                    if not os.path.exists(dirPath):
                        os.mkdir(dirPath,mode=0o777)
                    #need to have folder delete when rfp update feature is added
                    rfpFolderPath = os.path.join(dirPath,random_id)
                    os.mkdir(rfpFolderPath,mode=0o777)
                    rfpFilePath = os.path.join(rfpFolderPath,"file")
                    os.mkdir(rfpFilePath,mode=0o777)
                    filename = secure_filename(f.filename)
                    filePath = os.path.join(rfpFilePath, filename)
                    f.save(filePath)
                data['rfpFile'] = filename
                vendorsLst = []
                for i in data['oldVendorsList']:
                    vendorsLst.append(i['email'])
                for i in data['newVendorsList']:
                    vendorsLst.append(i)
                userName = ""
                for un in user.auth.find({"id":userId}):
                    userName = un['firstName']
                self.vendorsRFPmail(vendorsLst,data['bidEndDate'],random_id,userName)
                vendors.rfp.insert(data)
                response = {"status" : "success", "message":"RFP is created successfully and link is send to the respective vendors."}
                return response
            else:
                response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
                return response,status.HTTP_401_UNAUTHORIZED
        else:
            response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
            return response,status.HTTP_401_UNAUTHORIZED

    def vendorsRFPmail(self,vendorsList,closeDate,rfpId,buyer):
        expTime = {"time":closeDate}
        expTimeBytes = json.dumps(expTime).encode('ascii')
        eTime = b64encode(expTimeBytes).decode('ascii')
        sender = mailUserName
        subject = 'Vendor bidding'
        for vendor in vendorsList:
            try:
                html = "<p>Dear Vendor,</p><p>Welcome to the procurement world!</p><p>You have recieved a proposal request from "+buyer+". If you are registered vendor, Please click on <a href='"+host+"vendor/rfp/" + rfpId +"?t="+eTime+"'> here </a> to see the detailed information. </p><p>Don't you have an account? Please click on<a href='"+host+"'> here </a>to create free account.</p><br><p>Warm Regards,</p><p>Procurement Team</p>"
                message = 'click on the link to bid http://localhost:8082/vendor/rfp/'+rfpId +"?t="+eTime
                msg = Message(subject, sender =sender, recipients = [vendor])
                msg.html = html
                def sendAsyncMail(msg):
                    with app.app_context():
                        try:
                            mail.send(msg)
                            print("Successfully sent email")
                        except:
                            print("Error: unable to send email")
                mailThread = threading.Thread(name='mail_sender', target=sendAsyncMail, args=(msg,))
                mailThread.start()
                #mail.send(msg)
            except:
                print("Error: unable to send email 1")

class Bid(Resource):
    def post(self):
        if "access_key" in request.headers:
            accessKey = request.headers["access_key"]
            accessData = json.loads(b64decode(accessKey))
            print(type(accessData['id']))
            db = mongo.db
            if(db.logins.find(accessData)).count() == 1:
                print(request.form)
                data = json.loads(request.form.get('data'))
                print(data)
                rfpId = data['rfpId']
                vendorsList = data['vendors']
                for d in db.rfp.find({"rfp_id":rfpId}):
                    resObj = {"supplier":["Supplier"],"description":[d['itemDesc']],"quantity":["Quantity"],"unit":["Unit price"],"rate":["Total price"]}
                    oldVendorsList = d['oldVendorsList']
                    buyerDescription = d['itemDesc']
                    for i in oldVendorsList:
                        if i['id'] in vendorsList:
                            cData = i['fileData']
                            desCount = len(i['fileData']['description'])
                            companyName = "test"
                            for company in db.users.find({'user_id':i['id']}):
                                companyName = company['companyDetails']['companyName']
                                cData['Company Name'] = companyName
                            rd = []
                            for di in range(0,desCount):
                                resObj['supplier'].append(companyName +"-"+str(di+1))
                            for dis in i['fileData']['description']:
                                resObj['description'].append(dis)
                            if 'quantity' in i['fileData']:
                                for q in i['fileData']['quantity']:
                                    resObj['quantity'].append(q)
                            if 'unit' in i['fileData']:
                                for u in i['fileData']['unit']:
                                    resObj['unit'].append(convertToInteger(u))
                            if 'quantity' in i['fileData'] and 'unit' in i['fileData']:
                                for qu in range(len(i['fileData']['quantity'])):
                                    qtoInt = convertToInteger(i['fileData']['quantity'][qu])
                                    utoInt = convertToInteger(i['fileData']['unit'][qu])
                                    resObj['rate'].append(qtoInt*utoInt)
                            elif 'rate' in i['fileData']:
                                for r in i['fileData']['rate']:
                                    resObj['rate'].append(r)

                                
                resData = []
                for k,v in resObj.items():
                    resData.append(v)
                resObjarr = list(resObj.keys())
                df = pd.DataFrame(resData)
                output = BytesIO()
                writer = pd.ExcelWriter(output, engine='xlsxwriter')
                df.to_excel(writer, sheet_name='Sheet1', header=False,index=False)
                workbook = writer.book
                worksheet = writer.sheets['Sheet1']
                header_fmt = workbook.add_format({'font_name': 'Arial', 'bold': True,'text_wrap': True})
                format = workbook.add_format({'text_wrap': True})
                currency_format = workbook.add_format({'num_format': '₹#,##0.00','font_name': 'Arial', 'bold': True})
                bold = workbook.add_format({'bold': True,'color': 'red'})
                # Setting the format column A-B width to 50.
                #worksheet.set_column('A:A', 20, header_fmt)
                worksheet.set_column('B:Z', 20, format)
                worksheet.set_column('A:A', 20, header_fmt)
                worksheet.set_row(1,150)
                for c in range(len(resObjarr)):
                    cObj = resObjarr[c]
                    if cObj == 'unit' or cObj == 'rate':
                        worksheet.set_row(c,20,currency_format)
                    elif cObj == 'description':
                        desData = resObj['description']
                        for desD in range(1,len(desData)):
                            #buyerDescription = "Laptop, Intel i5 8th Generation Processor, 8GB RAM, 256GB SSD, 1.8 KGS Weight, 3 Years International Warranty"
                            #initial_data=self.checker(buyerDescription,desData[desD])
                            initial_data=compareSpec(buyerDescription,desData[desD])
                            #initial_data=[initial_data]
                            final=[]
                            k=""
                            for i in initial_data:
                                spltArr = i.split(" ")
                                for txtData in spltArr:
                                    txtData=txtData.replace("</B>","")
                                    txtData=txtData.replace("<B>","myBoldtext",1)
                                    txtData=txtData.replace("<B>","")
                                    if (txtData.find("myBoldtext") != -1):
                                        txtData = txtData.replace("myBoldtext","")
                                        final.append(bold)
                                        final.append(" "+txtData)
                                        k=bold
                                    else:
                                        final.append(" "+txtData)
                                        k=txtData
                            #columnNo = excelColumns[desD]+'2'
                            #print(final)
                            if len(final) > 0:
                                worksheet.write_rich_string(1,desD,*final)
                            # for i in list_for_excel:
                            #     if(k%2==1):
                            #         final.append(bold)
                            #     final.append(i)
                            #     k=k+1
                            print(final)
                            worksheet.write_rich_string('B2',*final)
                
                writer.close()
                output.seek(0)
                #df.to_excel('./uploads/test_compare.xlsx',index=0)
                return send_file(output,attachment_filename='comparison.xlsx',as_attachment=True)
                    

            else:
                response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
                return response,status.HTTP_401_UNAUTHORIZED
        else:
            response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
            return response,status.HTTP_401_UNAUTHORIZED
    
    def get(self,rfp_id):
        if "access_key" in request.headers:
            accessKey = request.headers["access_key"]
            accessData = json.loads(b64decode(accessKey))
            user = mongo.db
            if(user.logins.find(accessData)).count() == 1:
                detailsData = self.getRfpDetails(rfp_id,accessData)
                return {"status":"success","data":detailsData}
            else:
                response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
                return response,status.HTTP_401_UNAUTHORIZED
        else:
            response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
            return response,status.HTTP_401_UNAUTHORIZED

    def getRfpDetails(self,rfp_id,accessData):
        db = mongo.db
        d = db.rfp.find({"rfp_id":rfp_id})
        rfNewData = {}
        for i in d:
            if i['user_id'] != accessData['id']:
                rfNewData['itemCode'] = i['itemCode']
                rfNewData['itemName'] = i['itemName']
                rfNewData['HSNCode'] = i['HSNCode']
                rfNewData['UNSPSCCode'] = i['UNSPSCCode']
                rfNewData['itemDesc'] = i['itemDesc']
                rfNewData['quantity'] = i['quantity']
                rfNewData['bidType'] = i['bidType']
                rfNewData['bidStartDate'] = i['bidStartDate']
                rfNewData['bidEndDate'] = i['bidEndDate']
                rfNewData['rfpFile'] = i['rfpFile']
                rfNewData['isExpiry'] = isExpiry(i['bidEndDate'])
                if 'uom' in i:
                    rfNewData['uom'] = i['uom']
                if 'partNo' in i:
                    rfNewData['partNo'] = i['partNo']
                if i['bidType'] == 'open':
                    bidDetails = {}
                    vendorsList = []
                    for lst in i['oldVendorsList']:
                        if "fileData" in lst:
                            if lst['id'] == accessData['id']:
                                bidDetails['bidDesc'] = lst['fileData']['description'][0]
                                bidDetails['bidQty'] = lst['fileData']['quantity'][0]
                                bidDetails['bidUnitPrice'] = lst['fileData']['unit'][0]
                                bidDetails['bidTotal'] = lst['fileData']['rate'][0]
                            else:
                                v = {}
                                v['name'] = "vendor"
                                if "description" in lst['fileData']:
                                    v["bidDesc"] = lst['fileData']['description'][0]
                                if "quantity" in lst['fileData']:
                                    v["bidQty"] = lst['fileData']['quantity'][0]
                                if "unit" in lst['fileData']:
                                    v["bidUnitPrice"] = lst['fileData']['unit'][0]
                                if "rate" in lst['fileData']:
                                    v["bidTotal"] = lst['fileData']['rate'][0]
                                vendorsList.append(v)
                
                    rfNewData['bidDetails'] = bidDetails
                    rfNewData['vendorsList'] = vendorsList
                else:
                    rfNewData['uploadFilename'] = ""
                    for lst in i['oldVendorsList']:
                        if "fileData" in lst:
                            if lst['id'] == accessData['id'] and 'uploadFilename' in lst:
                                rfNewData['uploadFilename'] = lst['uploadFilename']
            else:
                del i['_id']
                del i['newVendorsList']
                vArr = i.pop('oldVendorsList')
                vlArr = []
                i['isExpiry'] = isExpiry(i['bidEndDate'])
                for vl in vArr:
                    if 'fileData' in vl:
                        v = {}
                        v['id'] = vl['id']
                        v['email'] = vl['email']
                        v['name'] = vl['name']
                        if i['bidType'] == 'open':
                            if "description" in vl['fileData']:
                                v["bidDesc"] = vl['fileData']['description'][0]
                            if "quantity" in vl['fileData']:
                                v["bidQty"] = vl['fileData']['quantity'][0]
                            if "unit" in vl['fileData']:
                                v["bidUnitPrice"] = vl['fileData']['unit'][0]
                            if "rate" in vl['fileData']:
                                v["bidTotal"] = vl['fileData']['rate'][0]
                        else:
                            if i['isExpiry']:
                                v['uploadFilename'] = vl['uploadFilename']
                        vlArr.append(v)
                i['vendorsList'] = vlArr
                
                rfNewData = i
        return rfNewData

    # def compare_function(self,Actual_string,User_data):
    #     # toreplace = "<b>\g<0></b>"
    #     # candidate=set(User_data.split(' '))-set(Actual_string.split(' '))
    #     # splitted_token=User_data.split(' ')
    #     # removal=[]
    #     # addition=[]
    #     # for i in range(len(splitted_token)):
    #     #     if (splitted_token[i].lower() in keywords or splitted_token[i] in keywords):
    #     #         if(splitted_token[i-1] in list(candidate)):
    #     #             removal.append(splitted_token[i-1])
    #     #             addition.append(splitted_token[i-1]+' '+splitted_token[i])
    #     #         elif(splitted_token[i+1] in list(candidate)):
    #     #             removal.append(splitted_token[i+1])
    #     #             addition.append(splitted_token[i]+' '+splitted_token[i+1])
    #     # remained=[i for i in candidate if i not in removal]
    #     # remained.extend(addition)
    #     # for i in remained: 
    #     #     if i.isalnum():
    #     #         User_data=(re.sub(i,toreplace, User_data))
    #     # return User_data
    #     actual_data_list=Actual_string.split('. ')
    #     user_data_list=User_data.split('. ')
    #     ## comparing similarity of senetces
    #     ratios=[]
    #     for i in actual_data_list:
    #         for j in user_data_list:
    #             ratios.append(fuzz.ratio(i,j))
    #     j=ratios.index(max(ratios))
    #     sentence_to_compare=user_data_list[j]
    #     Actual_string=actual_data_list[0]
    #     Actual_string=re.sub(',','',Actual_string)
    #     User_data=sentence_to_compare
    #     User_data=re.sub(',','',User_data)
    #     Actual_list=Actual_string.split(' ')
    #     user_list=User_data.split(' ')
    #     comparison=list(set(user_list)-set(Actual_list))
    #     consideration=self.words_identification(comparison)
    #     consideration = sorted(consideration, key=len,reverse=True)
    #     toreplace = "<b>\g<0></b>"
    #     initial_data=User_data
    #     for i in consideration:
    #         User_data=(re.sub(i,toreplace, User_data))
    #     # print(User_data)
    #     return User_data

    # ## Functions for word identification
    # def words_identification(self,comparison):
    #     consideration=[]
    #     for i in comparison:
    #         if(len(i)>0):
    #             if ((len(i)==2) and (i[-2].isalpha() and i[-1].isalpha() and i[-1]=='B' )):
    #                 pass
    #             elif ((i[0].isnumeric())and (i[-1].isalpha()) and i[-1]!='h'):
    #                 i=re.sub('[^0-9]','',i)
    #                 consideration.append(i)
    #             else:
    #                 consideration.append(i)
    #     print(consideration)
    #     return consideration

    # Function to return all uncommon words 
    def UncommonWords(self,A, B): 
        # count will contain all the word counts 
        count = {} 
        # insert words of string A to hash 
        # for word in A.split(): 
        #     count[word] = count.get(word, 0) + 1
        
        # insert words of string B to hash 
        for word in B.split(): 
            count[word] = count.get(word, 0) + 1
    
        # for word in C.split(): 
        #     count[word] = count.get(word, 0) + 1
        # # return required list of words 
        return [word for word in count if count[word] == 1] 
    
    
    def checker(self,A,B):
        querywordsA = A.split()
        querywordsB = B.split()
        # querywordsC = C.split()
        resultwords  = [word for word in querywordsA if word.lower() not in stopwords]
        newA = ' '.join(resultwords)
        resultwords  = [word for word in querywordsB if word.lower() not in stopwords]
        newB = ' '.join(resultwords)
        # resultwords  = [word for word in querywordsC if word.lower() not in stopwords]
        # newC = ' '.join(resultwords)
        # Print required answer 
        return (self.UncommonWords(newA, newB))

    
        

class List(Resource):
    def get(self):
        if "access_key" in request.headers:
            accessKey = request.headers["access_key"]
            accessData = json.loads(b64decode(accessKey))
            user = mongo.db
            if(user.logins.find(accessData)).count() == 1:
                db = mongo.db
                totalCount = 0
                ts = calendar.timegm(time.gmtime())
                print(ts)
                # request.args
                pageNumber = int(request.args.get('pageNumber'))
                nPerPage = int(request.args.get('count'))
                status = request.args.get('status')
                if accessData['accountType'] == "buyer":
                    if status == "all":
                        d = db.rfp.find({"user_id":accessData['id']}).sort("bidStartDate",-1).skip((pageNumber-1)*nPerPage).limit(nPerPage)
                    else:
                        d = db.rfp.find({'$and': [{"user_id":accessData['id']},{"bidEndDate":{"$lt":str(ts)}}]}).sort("bidStartDate",-1).skip((pageNumber-1)*nPerPage).limit(nPerPage)
                    totalCount = d.count()
                else:
                    if status == "all":
                        d = db.rfp.find().sort("bidStartDate",-1) 
                    else:
                        d = db.rfp.find({"bidEndDate":{"$lt":str(ts)}}).sort("bidStartDate",-1)
                    # skip((pageNumber-1)*nPerPage).limit(nPerPage)
                data = []
                for i in d:
                    # response = {"status":"error","message":"Please complete the registration form to continue."}
                    # return response
                    # if db.users.find({"user_id":accessData['id']}).count() == 0:
                    #     response = {"status":"error","message":"Please complete the registration form to continue."}
                    #     return response
                    if accessData['id'] == i['user_id']:
                        del i['_id']
                        i['isExpiry'] = isExpiry(i['bidEndDate'])
                        data.append(i)
                    else:
                        for oldVendor in i['oldVendorsList']:
                            if oldVendor['id'] == accessData['id']:
                                rfNewData = {}
                                rfNewData['id'] = i['rfp_id']
                                rfNewData['itemCode'] = i['itemCode']
                                rfNewData['itemName'] = i['itemName']
                                rfNewData['HSNCode'] = i['HSNCode']
                                rfNewData['UNSPSCCode'] = i['UNSPSCCode']
                                rfNewData['itemDesc'] = i['itemDesc']
                                rfNewData['quantity'] = i['quantity']
                                rfNewData['bidType'] = i['bidType']
                                rfNewData['bidStartDate'] = i['bidStartDate']
                                rfNewData['bidEndDate'] = i['bidEndDate']
                                rfNewData['isExpiry'] = isExpiry(i['bidEndDate'])
                                if 'uom' in i:
                                    rfNewData['uom'] = i['uom']
                                if 'partNo' in i:
                                    rfNewData['partNo'] = i['partNo']
                                data.append(rfNewData)

                resData = []
                if accessData['accountType'] == "vendor":
                    totalCount = len(data)
                    startIndex = (pageNumber-1)*nPerPage
                    endIndex = startIndex + nPerPage
                    if endIndex > totalCount:
                        endIndex = totalCount
                    for d in range(startIndex,endIndex):
                        resData.append(data[d])
                else:
                    resData = data

                response = {"status":"success","data":resData,"total":totalCount}
                return response
            else:
                response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
                return response,status.HTTP_401_UNAUTHORIZED
        else:
            response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
            return response,status.HTTP_401_UNAUTHORIZED

    def post(self):
        data = json.loads(request.form['data'])
        if "access_key" in request.headers:
            accessKey = request.headers["access_key"]
            accessData = json.loads(b64decode(accessKey))
            user = mongo.db
            if(user.logins.find(accessData)).count() == 1:
                userId = accessData['id']
                rfpId = data['rfp_id']
                filename = ""
                if "bidFile" in request.files:
                    f = request.files['bidFile']
                    dirPath = os.path.join(UPLOADS_PATH +userId,rfpId)
                    if os.path.exists(dirPath):
                        shutil.rmtree(dirPath)
                    os.mkdir(dirPath,mode=0o777)
                    filename = secure_filename(f.filename)
                    filePath = os.path.join(dirPath, filename)
                    f.save(filePath)
                    if (filename.find('.pdf') != -1):
                        df=pd.DataFrame([])
                        df_combine=pd.DataFrame([])
                        tables = camelot.read_pdf(filePath, pages = "1-end")
                        if len(tables) == 1:
                            df_combine = tables[0].df
                        else:
                            for tc in range(0,len(tables)-1):
                                df = tables[tc].df
                                df_combine = pd.concat([df_combine,df])
                        #tabCount = tabula.read_pdf(filePath, pages='all')
                        #print(tabCount)
                        if(len(tables) == 0):
                            return {"status":"error","message":"Data should be in table format"}
                        # if not os.path.exists(dirPath):
                        #     os.mkdir(dirPath,mode=0o777)
                        # tables.export(dirPath +"/" +"output.csv",f="csv")
                        #data = pd.read_csv(dirPath +"/" +"output.csv",header=None)
                        #print("cols>>>>>>>>>>>",list(data.columns))
                        #df = pd.DataFrame(data)
                        print(df_combine)
                        extData = self.fileExtractionPDF(df_combine.transpose())
                        #data = tabula.read_pdf(filePath,encoding='utf-8', pages='1-6041')
                        #data.to_csv(dirPath+"/output.csv", encoding='utf-8')
                        # extData = {}
                        # for i in range(0,len(data)-1):
                        #     print(data[i])
                        #     eData = self.fileExtraction(data[i])
                        #     for keys,vals in eData.items():
                        #         if keys in extData:
                        #             extData[keys].append(vals)
                        #         else:
                        #             extData[keys] = vals
                        # print(extData)
                    elif (filename.find('.xls') != -1):
                        data = pd.read_excel(filePath,header=None)
                        df = pd.DataFrame(data)
                        extData = self.fileExtractionXL(df)
                        if len(list(extData.keys())) == 0 or len(extData["description"]) == 0:
                            extData = self.fileExtractionPDF(df.transpose())
                else:
                    extData = {}
                    extData["description"] = [data["bidDesc"]]
                    extData["quantity"] = [data["bidQty"]]
                    extData["unit"] = [data["bidUnitPrice"]]
                    extData["rate"] = [data["bidTotal"]]

                for rfp in user.rfp.find({"rfp_id":rfpId}):
                    for i in rfp['oldVendorsList']:
                        if i['id'] == userId:
                            v = {}
                            v['id'] = i['id']
                            v['email'] = i['email']
                            v['name'] = i['name']
                            v['fileData'] = extData
                            v['uploadFilename'] = filename
                            user.rfp.update_one({'_id':rfp['_id'],'oldVendorsList':i},{'$set':{'oldVendorsList.$':v}},upsert=False)
                        
                return({"status":"success","message":"Bid placed successfully"})
                
            else:
                response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
                return response,status.HTTP_401_UNAUTHORIZED
        else:
            response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
            return response,status.HTTP_401_UNAUTHORIZED

    def fileExtractionPDF(self,df):
        number_of_rows = df.shape[0]
        number_of_columns = df.shape[1]
        objData = {}
        matchedObj = {}
        docdata = {}
        for row in range(number_of_rows):
            for col in range(number_of_columns):
                value  = (df.iloc[row,col])
                if (not isinstance(value,float) or (isinstance(value,float) and math.isnan(value) != True)):
                    value = str(value)
                    try:
                        ins = False
                        for key,val in textPhrases.items():
                            strVal = value.lower().replace('\n','')
                            if strVal in val:
                                obj = {}
                                matchedObj[key] = str(row)+"_"+str(col)
                                ins = True

                        if ins == False:
                            objData[str(row) +"_" +str(col)] = value.replace('\n', ' ').replace('\r', '')
                    except:
                        print("unexpected value",type(value))

        #columns = []
        print(objData)
        print(matchedObj)
        # for okey,oval in objData.items():
        #     splObjText = okey.split('_')
        #     columns.append(splObjText[1])
        for mkey,mval in matchedObj.items():
            splText = mval.split('_')
            docdata[mkey] = []
            dataText = ""
            for okey,oval in objData.items():
                splObjText = okey.split('_')
                if(splObjText[0] == splText[0]):
                    docdata[mkey].append(oval)
            

        print(objData)
        # print(matchedObj)
        print(docdata)
        return docdata


    def fileExtractionXL(self,df):
        number_of_rows = df.shape[0]
        number_of_columns = df.shape[1]
        objData = {}
        matchedObj = {}
        docdata = {}
        for row in range(number_of_rows):
            for col in range(number_of_columns):
                value  = (df.iloc[row,col])
                if (not isinstance(value,float) or (isinstance(value,float) and math.isnan(value) != True)):
                    value = str(value)
                    try:
                        ins = False
                        for key,val in textPhrases.items():
                            if value.lower() in val:
                                obj = {}
                                matchedObj[key] = str(row)+"_"+str(col)
                                ins = True

                        if ins == False:
                            objData[str(row) +"_" +str(col)] = value
                    except:
                        print("unexpected value",type(value))

        columns = []
        for okey,oval in objData.items():
            splObjText = okey.split('_')
            columns.append(splObjText[1])
        
        for mkey,mval in matchedObj.items():
            splText = mval.split('_')
            docdata[mkey] = []
            dataText = ""
            for okey,oval in objData.items():
                splObjText = okey.split('_')
                if((splObjText[1] == splText[1]) and (oval)):
                    ap = True
                    for t in range(0,len(columns)):
                        rcitem = (splObjText[0]+"_"+columns[t])
                        if not rcitem in objData:
                            ap = False
                        
                    if ap:
                        if dataText != "":
                            docdata[mkey].append(dataText)
                            dataText = ""
                        dataText = oval
                    else:
                        if dataText != "":
                            dataText = dataText +" " +oval
            if dataText != "":
                docdata[mkey].append(dataText)

        # dv = docdata.values()
        # dvItr = iter(dv)
        # rData = next(dvItr)
        # remColumnData = []
        # for k in rData:
        #     kSplt = k.split("_")
        #     remColumnData.append(int(kSplt[0]))
        # remColumnData.append(number_of_rows)

        # for kdoc,vdoc in docdata.items():
        #     for i in range(0,len(remColumnData)-1):

        # print(rData)
        print(objData)
        # print(matchedObj)
        print(docdata)
        dfData = pd.DataFrame(docdata)
        #dfData.to_csv('./uploads/test.csv')
        return docdata


class Password(Resource):
    def post(self):
        if "access_key" in request.headers:
            accessKey = request.headers["access_key"]
            accessData = json.loads(b64decode(accessKey))
            user = mongo.db
            if(user.logins.find(accessData)).count() == 1:
                for userData in user.auth.find({"email":accessData['email']}):
                    data = json.loads(request.data)
                    if data['oldPassword'] == userData['password']:
                        user.auth.update_one({'_id':userData['_id']},{'$set':{'password':data['newPassword']}},upsert=False)
                        receivers = [userData['email']]
                        message = "Password update"
                        html = "<p>Dear "+ userData['firstName']+ ",</p><p>Your password has been updated successfully.</p><p>Please use your new password for further login</p><br><p>Warm Regards,</p><p>Procurement Team</p>"
                        sendMail(receivers,message,html)
                        response = {"status":"success","message":"Password has been changed successfully"}
                    else:
                        response = {"status":"error","message":"Old password is incorrect"}
                    return response
            else:
                response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
                return response,status.HTTP_401_UNAUTHORIZED

        elif len(request.args.get('e')) > 0:
            email = b64decode(request.args.get('e')).decode("utf-8")
            user = mongo.db
            for userData in user.auth.find({"email":email}):
                data = json.loads(request.data)
                user.auth.update_one({'_id':userData['_id']},{'$set':{'password':data['newPassword']}},upsert=False)
                response = {"status":"success","message":"Password has been changed successfully"}
                return response
            response = {"status":"error","message":"Password not updated"}
            return response
        else:
            response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
            return response,status.HTTP_401_UNAUTHORIZED

class ForgetPassword(Resource):
    def post(self):
        data = json.loads(request.data)
        user = mongo.db
        if user.auth.find({'email':data['email']}).count() == 1:
            for userData in user.auth.find({'email':data['email']}):
                mailEn = userData['email'].encode('ascii')
                mailEncode = b64encode(mailEn).decode('ascii')
                receivers = [userData['email']]
                message = "Password update link"
                html = "<p>Dear "+ userData['firstName']+ ",</p><p>Please click <a href='"+host+"reset-password/"+mailEncode+"'> here </a> to reset your new password.</p><br><p>Warm Regards,</p><p>Procurement Team</p>"
                sendMail(receivers,message,html)
                response = {"status":"success","message":"Password set mail has been sent to your mail id"}
        else:
            response = {"status":"error","message":"This email is not registered"}
        return response

class GetUNPSC(Resource):
    def get(self):
        if "access_key" in request.headers:
            accessKey = request.headers["access_key"]
            accessData = json.loads(b64decode(accessKey))
            user = mongo.db
            if(user.logins.find(accessData)).count() == 1:
                with open('unpsc.json') as f:
                    data = json.load(f)
                    response = {"status":"success","total":len(data),"data":data}
                    return response
            else:
                response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
                return response,status.HTTP_401_UNAUTHORIZED
        else:
            response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
            return response,status.HTTP_401_UNAUTHORIZED

class GetHSN(Resource):
    def get(self):
        if "access_key" in request.headers:
            accessKey = request.headers["access_key"]
            accessData = json.loads(b64decode(accessKey))
            user = mongo.db
            if(user.logins.find(accessData)).count() == 1:
                with open('hsn.json') as f:
                    data = json.load(f)
                    response = {"status":"success","total":len(data),"data":data}
                    return response
            else:
                response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
                return response,status.HTTP_401_UNAUTHORIZED
        else:
            response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
            return response,status.HTTP_401_UNAUTHORIZED

class BidFile(Resource):
    def post(self):
        if "access_key" in request.headers:
            accessKey = request.headers["access_key"]
            accessData = json.loads(b64decode(accessKey))
            user = mongo.db
            if(user.logins.find(accessData)).count() == 1:
                data = json.loads(request.data)
                rfpId = data['rfp_id']
                if 'user_id' in data:
                    userId = data['user_id']
                else:
                    userId = accessData['id']
                dirPath = os.path.join(UPLOADS_PATH, userId)
                filePath = os.path.join(dirPath,rfpId)
                for r, d, f in os.walk(filePath):
                    for file in f:
                        if file != "output.csv":
                            return send_file(os.path.join(filePath,file),attachment_filename=file,as_attachment=True)
                response = {"status":"error","error_message":"File not found"}
                return response
            else:
                response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
                return response,status.HTTP_401_UNAUTHORIZED
        else:
            response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
            return response,status.HTTP_401_UNAUTHORIZED

class RFPFile(Resource):
    def post(self):
        if "access_key" in request.headers:
            accessKey = request.headers["access_key"]
            accessData = json.loads(b64decode(accessKey))
            user = mongo.db
            if(user.logins.find(accessData)).count() == 1:
                data = json.loads(request.data)
                rfpId = data['rfp_id']
                if user.rfp.find({"rfp_id":rfpId}).count() > 0:
                    for uid in user.rfp.find({"rfp_id":rfpId}):
                        userId = uid['user_id']
                else:
                    response = {"status":"error","error_message":"File not found"}
                    return response
                dirPath = os.path.join(UPLOADS_PATH, userId)
                folderPath = os.path.join(dirPath,rfpId)
                filePath = os.path.join(folderPath,"file")
                for r, d, f in os.walk(filePath):
                    for file in f:
                        return send_file(os.path.join(filePath,file),attachment_filename=file,as_attachment=True)
                response = {"status":"error","error_message":"File not found"}
                return response
            else:
                response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
                return response,status.HTTP_401_UNAUTHORIZED
        else:
            response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
            return response,status.HTTP_401_UNAUTHORIZED

class RFPPurchase(Resource):
    def post(self,rfpId):
        if "access_key" in request.headers:
            accessKey = request.headers["access_key"]
            accessData = json.loads(b64decode(accessKey))
            user = mongo.db
            data = json.loads(request.data)
            if(user.logins.find(accessData)).count() == 1:
                for rfpData in user.rfp.find({"rfp_id":rfpId}):
                    vendorsList = rfpData['oldVendorsList']
                    vList = data['vendorsList']
                    vArr = []
                    for vl in rfpData['oldVendorsList']:
                        if vl['id'] in vList:
                            fileData = vl['fileData']
                            for i in range(0,len(fileData['description'])):
                                ls = {"id":vl['id'],"name":vl['name'],"email":vl['email'],"description":"","quantity":"","unit":"","total":""}
                                ls['description'] = fileData['description'][i]
                                if 'quantity' in fileData:
                                    ls['quantity'] = fileData['quantity'][i]
                                if 'unit' in fileData:
                                    ls['unit'] = fileData['unit'][i]
                                if 'quantity' in fileData and 'unit' in fileData:
                                    qtoInt = convertToInteger(fileData['quantity'][i])
                                    utoInt = convertToInteger(fileData['unit'][i])
                                    ls['total'] = str(qtoInt*utoInt)
                                vArr.append(ls)

                    response = {"status":"success","data":vArr}
                    return response
            else:
                response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
                return response,status.HTTP_401_UNAUTHORIZED
        else:
            response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
            return response,status.HTTP_401_UNAUTHORIZED

class VendorDetail(Resource):
    def get(self,vendorId):
        if "access_key" in request.headers:
            accessKey = request.headers["access_key"]
            accessData = json.loads(b64decode(accessKey))
            user = mongo.db
            if(user.logins.find(accessData)).count() == 1:
                for rfpData in user.users.find({"user_id":vendorId}):
                    del rfpData['_id']
                    data = {}
                    data['companyDetails'] = rfpData['companyDetails']
                    data['companyDetails']['statGSTIn'] = rfpData['statutoryDetails']['statGSTIn']
                    response = {"status":"success","data":data}
                    return response
            else:
                response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
                return response,status.HTTP_401_UNAUTHORIZED
        else:
            response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
            return response,status.HTTP_401_UNAUTHORIZED

class purchaseMail(Resource):
    def post(self):
        if "access_key" in request.headers:
            accessKey = request.headers["access_key"]
            accessData = json.loads(b64decode(accessKey))
            user = mongo.db
            data = json.loads(request.data)
            print(data)
            if(user.logins.find(accessData)).count() == 1:
                message = "Purchase order"
                receivers = [data['to_mail_id']]
                pdf = pdfkit.from_string(data['html'], 'out.pdf')
                sendMail(receivers,message,data['html'])
                response = {"status":"success","data":"Mail sent successfully"}
                return response
            else:
                response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
                return response,status.HTTP_401_UNAUTHORIZED
        else:
            response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
            return response,status.HTTP_401_UNAUTHORIZED

class Profile(Resource):
    def get(self):
        if "access_key" in request.headers:
            accessKey = request.headers["access_key"]
            accessData = json.loads(b64decode(accessKey))
            db = mongo.db
            if(db.logins.find(accessData)).count() == 1:
                print(db.users.find({"user_id":accessData['id']}))
                if db.users.find({"user_id":accessData['id']}).count() == 1:
                    for data in db.users.find({"user_id":accessData['id']}):
                        del data['_id']
                else:
                    data = {}
                response = {"status":"success","data":data}
                return response
            else:
                response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
                return response,status.HTTP_401_UNAUTHORIZED
        else:
            response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
            return response,status.HTTP_401_UNAUTHORIZED
    def post(self):
        if "access_key" in request.headers:
            accessKey = request.headers["access_key"]
            accessData = json.loads(b64decode(accessKey))
            db = mongo.db
            data = json.loads(request.data)
            print(data)
            if(db.logins.find(accessData)).count() == 1:
                data['user_id'] = accessData['id']
                db.users.insert(data)
                response = {"status":"success","data":"Profile save successfully"}
                return response
            else:
                response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
                return response,status.HTTP_401_UNAUTHORIZED
        else:
            response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
            return response,status.HTTP_401_UNAUTHORIZED

class VendorBidding(Resource):
    def post(self):
        if "access_key" in request.headers:
            accessKey = request.headers["access_key"]
            accessData = json.loads(b64decode(accessKey))
            user = mongo.db
            data = json.loads(request.data)
            print(data)
            if(user.logins.find(accessData)).count() == 1:
                rfpId = data["rfp_id"]
                vArr = []
                for rfp in user.rfp.find({"rfp_id":rfpId}):
                    for vl in rfp['oldVendorsList']:
                        if vl['id'] == data["id"]:
                            fileData = vl['fileData']
                            for i in range(0,len(fileData['description'])):
                                ls = {"id":vl['id'],"name":vl['name'],"email":vl['email'],"description":"","quantity":"","unit":"","total":""}
                                ls['description'] = fileData['description'][i]
                                if 'quantity' in fileData:
                                    ls['quantity'] = fileData['quantity'][i]
                                if 'unit' in fileData:
                                    ls['unit'] = fileData['unit'][i]
                                if 'quantity' in fileData and 'unit' in fileData:
                                    qtoInt = convertToInteger(fileData['quantity'][i])
                                    utoInt = convertToInteger(fileData['unit'][i])
                                    ls['total'] = str(qtoInt*utoInt)
                                vArr.append(ls)

                response = {"status":"success","data":vArr}
                return response
            else:
                response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
                return response,status.HTTP_401_UNAUTHORIZED
        else:
            response = {"status" : "error", "error_code":401,"error_message":"Unauthorized"}
            return response,status.HTTP_401_UNAUTHORIZED

api.add_resource(Login,'/user/login','/user/logout')
api.add_resource(Users,'/user/create', '/user/confirm/<string:id>')
api.add_resource(Vendor, '/vendor/register','/vendor/list', endpoint="user")
api.add_resource(RFP, '/rfp/create', '/rfp/create/<string:rfp_id>' ,endpoint="rfp")
api.add_resource(Bid, '/rfp/details/<string:rfp_id>','/rfp/bid/compare')
api.add_resource(List,'/rfp/list','/rfp/bid')
api.add_resource(Password,'/user/password/change')
api.add_resource(ForgetPassword,'/user/forgetpassword')
api.add_resource(GetUNPSC,'/getunspsc')
api.add_resource(GetHSN,'/gethsn')
api.add_resource(BidFile,'/bid/file/download')
api.add_resource(RFPFile,'/rfp/file/download')
api.add_resource(RFPPurchase,'/rfp/purchase/<string:rfpId>')
api.add_resource(VendorDetail,'/vendor/detail/<string:vendorId>')
api.add_resource(purchaseMail,'/purchase/mail')
api.add_resource(Profile,'/user/profile','/user/getprofile')
api.add_resource(VendorBidding,'/vendor/biddetail')

if __name__ == '__main__':
    #context = ('server.crt', 'server.key')
    #app.run(host='0.0.0.0',port=5002, debug=True, ssl_context=context)
    app.run(port=5002, debug=True)