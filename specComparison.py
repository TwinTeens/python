
from difflib import get_close_matches

def Addtional_spec_buyer(list1,list2):
    return (list(set(list1)-set(list2)))

def strike(text):
    #print(type(text))
    toBeStriked = text + ' '
    return ''.join([u'\u0336{}'.format(c) for c in toBeStriked])

def remove_special_char(string):
    special_char = [',',':']
    for i in special_char:
        string = string.replace(i,' ')
    return string

def remove_white_spaces(any_list):
    sample = []
    for i in any_list:
        sample.append(i.strip())
    return sample

def change_listto_string(list):
    str = " "
    return(str.join(list))

def word_in_buyerlist(word,list):
    # check if a given word(in supplier spec) is in buyer's spec
    if word in list:
        return True
    else:
        #print(word)
        return False

def list_ch(list1,list2):
    count = 0
    #print(list2)
    for item in list1:
        check = word_in_buyerlist(item,list2)
        #print(check)
        if check != True:
            #print(item,count)
            res = get_close_matches(item,list2,1,0.6)
            #print(res,count,item)
            if res:
                # This will separate partially same specs
                # Use the below code only if you need to use ANSI value to change colour to purple
                #if count == 0:
                    #item = item.capitalize()
                #col_item = "\033[95m {}\033[00m" .format(item) #purple colour text
                col_item = "<B>{}</B>".format(item)
                #print(col_item.upper())
                list1[count] = col_item
            else:
                # This will detect any new spec data(spec details not in buyer's spec)
                # Use the below code only if you need to use ANSI value to change colour to green
                #if count == 0:
                    #item = item.capitalize()
                #col_item = "\033[92m {}\033[00m" .format(item) #Green colour text
                col_item = "<B>{}</B>".format(item)
                list1[count] = col_item



        count+=1
    return list1

def compareSpec(buyersSpecSentence,supplierSpecSentence):
    b_lower = buyersSpecSentence.lower()
    s_lower = supplierSpecSentence.lower()
    b_spec = remove_special_char(b_lower)
    #print(b_spec)
    s_spec = remove_special_char(s_lower)
    b_Split_Tokens = b_spec.split(' ')
    s_Split_Tokens = s_spec.split(' ')
    s_SpecTokens = remove_white_spaces(s_Split_Tokens)
    b_SpecTokens = remove_white_spaces(b_Split_Tokens)
    output_spec = '';
    striked_item = '';
#print(b_SpecTokens)
#print(s_SpecTokens)
#print("list result:" , list_comparison(s_SpecTokens,b_SpecTokens))
    result_list = list_ch(s_SpecTokens,b_SpecTokens)
    print(result_list)
    #output_spec = change_listto_string(result_list)
    #output_spec2 = Addtional_spec_buyer(b_SpecTokens,s_SpecTokens)
    # supplier's spec - returned as a string
    #print("The compared supplier's statement:\n ",format(output_spec))
    # missing spec - returned as a list
    #print("Missing specs:")
    # for item in output_spec2:
    #     striked_item += strike(item) + '\n'
        #print(striked_item)
        

    return result_list 
